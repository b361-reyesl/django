from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone

# Local imports
from .models import GroceryItem, Event
from .forms import LoginForm, AddEventForm, RegisterForm, AddGroceryForm
# AddItemForm, UpdateItemForm

# Create your views here.
def index(request):
    groceryitem_list = GroceryItem.objects.all()
    event_list = Event.objects.all()
    context = {'groceryitem_list':groceryitem_list, 'event_list': event_list}
    return render(request, "django_practice/index.html", context)

def groceryitem(request, groceryitem_id):
    groceryitem = get_object_or_404(GroceryItem, pk = groceryitem_id)
    return render(request, "django_practice/groceryitem.html", model_to_dict(groceryitem))

def event(request, event_id):
    event = get_object_or_404(Event, pk = event_id)
    return render(request, "django_practice/event.html", model_to_dict(event))

def register(request):
    context = {}

    if request.method == 'POST':
        form = RegisterForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            
            if User.objects.filter(username=username).exists():
                context['error'] = True
            else:
                user = User.objects.create_user(username=username, first_name=first_name, last_name=last_name, email=email, password=password)
                user.is_staff = False
                user.is_active = True
                user.save()
                return redirect('django_practice:login')
    else:
        form = RegisterForm()

    context['form'] = form

    return render(request, "django_practice/register.html", context)


def change_password(request):
    is_user_authenticated = False
    user = authenticate(username = "johndoe", password = "john1234")
    print(user)

    if user is not None:
        authenticated_user = User.objects.get(username = 'johndoe')
        authenticated_user.set_password("johndoe12")
        authenticated_user.save()
        is_user_authenticated = True
    context = {
            "is_user_authenticated" : is_user_authenticated
        }

    return render(request, "django_practice/change_password.html", context)
    
def add_event(request):
    context = {}

    if request.method == 'POST':
        form = AddEventForm(request.POST)

        if form.is_valid():
            event_name = form.cleaned_data['event_name']
            description = form.cleaned_data['description']

            duplicates = Event.objects.filter(event_name=event_name)

            if not duplicates:
                Event.objects.create(
                    event_name=event_name,
                    description=description,
                    date_created=timezone.now(),
                    user=request.user
                )
                return redirect('django_practice:index')
            else:
                context['error'] = True
        else:
            context['form'] = form
    return render(request, "django_practice/add_event.html", context)

def add_grocery(request):
    context = {}

    if request.method == 'POST':
        form = AddGroceryForm(request.POST)

        if form.is_valid():
            item_name = form.cleaned_data['item_name']
            category = form.cleaned_data['category']

            duplicates = GroceryItem.objects.filter(item_name=item_name)
    
            if not duplicates:
                GroceryItem.objects.create(
                    item_name=item_name,
                    category=category,
                    date_created=timezone.now(),
                    user=request.user
                )
            else:
                context = {
                    "error": True
                }
        else:
            context['form'] = form
    return render(request, "django_practice/add_grocery.html", context)

def login_view(request):
    context = {}
    
    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid() == False:
            form = LoginForm()
        else:
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            context = {
                "username": username,
                "password": password
            }

        if user is not None:
            login(request, user)
            return redirect("django_practice:index")
        else:
            context = {
                "error": True
            }

    return render(request, "django_practice/login.html", context)

def logout_view(request):
    logout(request)
    return redirect("django_practice:index")

def delete_task(request, groceryitem_id):
    groceryitem = GroceryItem.objects.filter(pk=groceryitem_id).delete()
    return redirect("django_practice:index")

    